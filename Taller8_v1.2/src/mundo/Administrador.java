package mundo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Nodo;
import javafx.scene.shape.Arc;

/**
 * Clase que representa el administrador del sistema de metro de New York
 *
 */
public class Administrador {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";

	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";


	/**
	 * Grafo que modela el sistema de metro de New York
	 */
	//TODO Declare el atributo grafo No dirigido que va a modelar el sistema. 
	// Los identificadores de las estaciones son String
	private GrafoNoDirigido<String,String> grafo;

	private int canRutas;

	/**
	 * Construye un nuevo administrador del Sistema
	 */
	public Administrador() 
	{
		//TODO inicialice el grafo como un GrafoNoDirigido
		grafo = new GrafoNoDirigido<>();
	}

	/**
	 * Devuelve todas las rutas que pasan por la estacion con nombre dado
	 * @param identificador 
	 * @return Arreglo con los identificadores de las rutas que pasan por la estacion requerida
	 */
	public String[] darRutasEstacion(String identificador)
	{
		//TODO Implementar
		Arco<String, String>[] origen = grafo.darArcosOrigen(identificador);
		Arco<String, String>[] destino = grafo.darArcosDestino(identificador);
		String[] rutas = null;
		if(origen != null && destino!= null )
			rutas = new String[origen.length+destino.length];
		else if (origen!=null && destino == null )
			rutas = new String[origen.length];
		else if (origen ==null && destino != null )
			rutas = new String[destino.length];

		if(origen != null)
		{
			for(int i=0; i<origen.length;i++)
			{
				if(origen[i]!= null)
					rutas[i] = origen[i].darNodoFin().darId().toString();
			}
		}
		if (destino != null)
		{
			for(int j =0; j<destino.length;j++)
			{
				if(destino[j]!= null)
					rutas[j] = destino[j].darNodoFin().darId().toString();
			}
		}
		return rutas;



	}

	/**
	 * Devuelve la distancia que hay entre las estaciones mas cercanas del sistema.
	 * @return distancia minima entre 2 estaciones
	 */
	public double distanciaMinimaEstaciones()
	{
		//TODO Implementar
		Arco<String, String>[] arcos = grafo.darArcos();
		double min = 200000;
		for(int i=0; i<arcos.length; i++)
		{
			if(arcos[i].darCosto()<min)
				min= arcos[i].darCosto();
		}
		return min;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones más lejanas del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaEstaciones()
	{
		//TODO Implementar
		Arco<String, String>[] arcos = grafo.darArcos();
		double max = 0;
		for(int i=0; i<arcos.length; i++)
		{
			if(arcos[i].darCosto()>max)
				max= arcos[i].darCosto();
		}
		return max;
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		//TODO Implementar
		File paraderos = new File(RUTA_PARADEROS);
		if(paraderos.exists())
		{
			Scanner sc = new Scanner(new FileInputStream(paraderos));
			int num = sc.nextInt();
			int i =0;

			while(i<num)
			{
				String[] linea = sc.next().split(";");
				Double lat = Double.parseDouble(linea[1]);
				Double longi = Double.parseDouble(linea[2]);
				String id = linea[0];
				Estacion<String> estacion =new Estacion<String>(id, lat, longi);
				grafo.agregarNodo(estacion);
				i++;
			}
			sc.close();
		}
		System.out.println("Se han cargado correctamente "+grafo.darNodos().length+" Estaciones");

		//TODO Implementar

		File rutas = new File(RUTA_RUTAS);
		
		if(rutas.exists())
		{
			BufferedReader bf = new BufferedReader(new FileReader(rutas));
			int num = Integer.parseInt(bf.readLine());
			int j =0;
			while(j<num)
			{
				String nombreRuta = "";
				String linea =  bf.readLine();
				if(linea.startsWith("-"))
				{
					nombreRuta = bf.readLine();
					int numRutas = Integer.parseInt(bf.readLine());
					String ini = "";
					for(int k = 0; k<numRutas; k++)
					{
						String[] linea2 = bf.readLine().split(" ");
						String nombreEs = linea2[0];
						int distancia = Integer.parseInt(linea2[1]);
						if(distancia ==0)
						{
							ini = nombreEs; 
							continue;
						}
						grafo.agregarArco(ini, nombreEs, distancia, nombreRuta);
						canRutas++;
						grafo.agregarArco(nombreEs, ini, distancia, nombreRuta);
						canRutas++;
					}
				}
				j++;
			}
			bf.close();
		}
		grafo.cambiarCantRutas(canRutas);

		System.out.println("Se han cargado correctamente "+ grafo.darArcos().length+" arcos");
	
	}

	/**
	 * Busca la estacion con identificador dado<br>
	 * @param identificador de la estacion buscada
	 * @return estacion cuyo identificador coincide con el parametro, null de lo contrario
	 */
	public Estacion<String> buscarEstacion(String identificador)
	{
		//TODO Implementar
		Estacion<String> est = (Estacion<String>)grafo.buscarNodo(identificador);
		return est;
	}

}
