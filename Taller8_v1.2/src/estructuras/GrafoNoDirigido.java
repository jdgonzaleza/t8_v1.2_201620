package estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K,E> {

	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	private HashLinear<K, Nodo<K>> nodos;
	 

	/**
	 * Lista de adyacencia 
	 */
	private HashLinear<String, List<Arco<K,E>>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
		// Es libre de implementarlo con la representacion de su agrado. 
	private int cant;
	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		//TODO implementar
		adj = new HashLinear<>();
		nodos = new HashLinear<>();
	}

	@Override
	public boolean agregarNodo(Nodo<K> nodo) {
		//TODO implementar
		nodos.put(nodo.darId(), nodo);
		return nodos.contains(nodo.darId());
	}

	@Override
	public boolean eliminarNodo(K id) {
		//TODO implementar
		nodos.delete(id);
		return nodos.contains(id);
	}

	@Override
	public Arco<K,E>[] darArcos() {
		//TODO implementar
		Arco<K,E>[] arcos = new Arco[cant];
		int i = 0;
		for(String llave: adj.keys())
		{
			List<Arco<K, E>> act = adj.get(llave);
			for(Arco<K,E> c: act)
			{
				if(c!=null)
				{
					arcos[i]=c;
					i++;
				}
			}
		}
		return arcos;
	}

	private Arco<K,E> crearArco( K inicio, K fin, double costo, E e )
	{
		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if ( nodoI != null && nodoF != null)
		{
			return new Arco<K,E>( nodoI, nodoF, costo, e);
		}
		{
			return null;
		}
	}

	@Override
	public Nodo<K>[] darNodos() {
		//TODO implementar
		Nodo<K>[] nodosArr = new Nodo[nodos.size()];
		int i =0;
		for( K llave: nodos.keys())
		{
			nodosArr[i++] = nodos.get(llave);
		}
		return nodosArr;
		
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo, E obj) {
		//TODO implementar
		boolean resp= true;
		Arco<K, E> arco = crearArco(inicio, fin, costo, obj);
		List<Arco<K,E>> listaAcros = adj.get(inicio.toString());
		if(listaAcros != null)
		{
			listaAcros.add(arco);
		}
		else
		{
			List<Arco<K,E>> list = new LinkedList<>();
			list.add(arco);
			adj.put(inicio.toString(), list);
			resp =  adj.get(inicio.toString()).contains(arco);
		}
		return resp;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo) {
		return agregarArco(inicio, fin, costo, null);
	}

	@Override
	public Arco<K,E> eliminarArco(K inicio, K fin) {
		//TODO implementar
		
		List<Arco<K, E>> listaArcos = adj.get(inicio.toString());
		Arco<K, E> arcoElim = null;
		for(int i = 0; i< listaArcos.size(); i++)
		{
			Arco<K, E> act = listaArcos.get(i);
			if(act.darNodoFin().equals(fin))
			{
				arcoElim = listaArcos.remove(i);
				break;
			}
		}
		return arcoElim;
	}

	@Override
	public Nodo<K> buscarNodo(K id) {
		//TODO implementar
		return nodos.get(id);
	}

	@Override
	public Arco<K,E>[] darArcosOrigen(K id) {
		//TODO implementar
		
		List<Arco<K, E>> lista = adj.get(id.toString());
		
		if(lista != null)
		{
			Arco<K,E>[] arcos = new Arco[lista.size()];	
			for(int i =0; i < lista.size(); i++)
			{
				arcos[i] = lista.get(i);
			}
			return arcos;
		}
		else
			return null;
		
	}

	@Override
	public Arco<K,E>[] darArcosDestino(K id) {
		//TODO implementar
		Arco<K,E>[] resp = null;
		Arco<K,E>[] arc = darArcos();
		ArrayList<Arco<K,E>> temp = new ArrayList<>();
		for(int i =0; i<adj.size();i++)
		{
			if(arc[i].darNodoFin().equals(id))
			{
				temp.add(arc[i]);
			}
		}
		resp = new Arco[temp.size()];
		for(int i =0; i< temp.size();i++)
		{
			resp[i] = temp.get(i);
		}
		return resp;
	}
	
	public void cambiarCantRutas(int cantRutas)
	{
		cant = cantRutas;
	}

}
